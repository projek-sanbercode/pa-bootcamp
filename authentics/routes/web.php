<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SchduleController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TerimaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'HalamanLogin'])->name('/login');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

 Route::get('/', function () {
     return view('utama');
 });

 Route::get('/contactus', function () {
    return view('contactus');
});
Route::get('/hallogin', function () {
    return view('hallogin');
});

Route::get('/kembali', function () {
    return view('kembali');
});

Route::get('/halhome', function () {
    return view('halhome');
});

Route::get('/halhome', [RatingController::class, 'halhome'])->name('halhome');
Route::POST('/halcontact', [FeedbackController::class, 'feed'])->name('halcontact');

Route::get('/logout', function () {
    \Auth::logout();
    return redirect('home');
});

Route::get('/schdule', [SchduleController::class, 'index'])->name('schdule');
Route::post('/insertdata', [SchduleController::class, 'insertdata'])->name('insertdata');

Route::post('/insertdata', [SchduleController::class, 'halhome'])->name('insertdata');

Route::post('/tambahdata', [FeedbackController::class, 'tambahdata'])->name('tambahdata');
Route::post('/rating', [RatingController::class, 'tambahrating'])->name('rating');
Route::get('/rating', [RatingController::class, 'rating'])->name('rating');

Route::post('/tambahrating', [RatingController::class, 'tambahrating'])->name('tambahrating');
Route::post('/tambahrating', [RatingController::class, 'halhome'])->name('tambahrating');
Route::get('/Review', [ReviewController::class, 'review']);

Route::get('/daftar' , [ReviewController::class , 'daftar'])->name('daftar');

//baru jadwal

Route::get('/jadwal' , [JadwalController::class , 'index'])->name('index');
Route::post('/tambo' , [JadwalController::class , 'tambo'])->name('tambo');


Route::get('/dashboard' , [DashboardController::class , 'index'])->name('dashboard');
Route::get('/master' , [DashboardController::class , 'utama'])->name('utama');
Route::get('/tampil/{id}' , [TerimaController::class , 'tampil'])->name('tampil');
Route::post('/update/{id}' , [TerimaController::class , 'update'])->name('update');

Route::get('tolak/{id}' , [TerimaController::class , 'Tolak'])->name('Tolak');
Route::post('reject/{id}' , [TerimaController::class , 'reject'])->name('reject');







