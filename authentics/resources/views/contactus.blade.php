<body class="bg-emerald-400">
<header class="text-gray-600 body-font bg-stone-500">
    <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
      <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
        <img src="gambar/brand.png" alt="" width="50px">
        <span class="ml-3 text-xl text-gray-50">Barbershop</span>
      </a>
      <nav class="md:mr-auto md:ml-4 md:py-1 md:pl-4 md:border-l md:border-gray-400	flex flex-wrap items-center text-base justify-center">
        <a class="mr-5 hover:text-gray-900 text-gray-50 " href="/">Home</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50 " onclick="ShowAlert()">Make schedules</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50" href="/contactus">Contact us</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50" onclick="review()">Feedback</a>
      </nav>
      <button class="inline-flex text-white bg-yellow-300 border-0 py-1 px-4 focus:outline-none hover:bg-yellow-300 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-yellow-300 duration-300 transition ease-in-out delay-150 bg-yellow-300 hover:-translate-y-1 hover:scale-110 hover:bg-yellow-300 duration-300 m-1"><a href="/login">Login</a></button>
      <button class="inline-flex text-white bg-indigo-500 border-0 py-1 px-4 focus:outline-none hover:bg-indigo-600 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300 transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-teal-500 duration-300"><a href="/register">Register</a></button>
    </div>
  </header>
    <script src="https://cdn.tailwindcss.com"></script>
    <section class="text-gray-600 body-font relative">
      <div class="container px-5 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
        <div class="lg:w-2/3 md:w-1/2 bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-start relative">
        <iframe width="100%" height="100%" class="absolute inset-0" frameborder="0" title="map" marginheight="0" marginwidth="0" scrolling="no" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31856.8195266328!2d98.65938041562498!3d3.5638860000000046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1011454328738c69%3A0x4b0b4386e7ff3fa0!2sPerabot%20Agung!5e0!3m2!1sid!2sid!4v1672755969988!5m2!1sid!2sid" style="filter: grayscale(1) contrast(1.2) opacity(0.4);"></iframe>
          <div class="bg-white relative flex flex-wrap py-6 rounded shadow-md m-10">
            <div class="lg:w-1/2 px-6">
              <h2 class="title-font font-semibold text-gray-900 tracking-widest text-xs">ADDRESS</h2>
              <p class="mt-1">Perabot Agung, Jalan Sisingamangaraja nomor 126 Medan</p>
            </div>
            <div class="lg:w-1/2 px-6 mt-4 lg:mt-0">
              <h2 class="title-font font-semibold text-gray-900 tracking-widest text-xs">EMAIL</h2>
              <a class="text-indigo-500 leading-relaxed">Barber@email.com</a>
              <h2 class="title-font font-semibold text-gray-900 tracking-widest text-xs mt-4">PHONE</h2>
              <p class="leading-relaxed">0852-9675-8872</p>
            </div>
          </div>
        </div>
        <div class="lg:w-1/3 md:w-1/2 bg-white flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
          <h2 class="text-gray-900 text-lg mb-1 font-medium title-font m-1">Feedback</h2>
          <p class="leading-relaxed mb-5 text-gray-600 m-1">Post-ironic portland shabby chic echo park, banjo fashion axe</p>
          <div class="relative mb-4">
            <label for="name" class="leading-7 text-sm text-gray-600 m-1">Name</label>
            <input type="text" id="name" name="name" class="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
          </div>
          <div class="relative mb-4">
            <label for="email" class="leading-7 text-sm text-gray-600 m-1">Email</label>
            <input type="email" id="email" name="email" class="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
          </div>
          <div class="relative mb-4">
            <label for="message" class="leading-7 text-sm text-gray-600 m-1">Message</label>
            <textarea id="message" name="message" class="w-full bg-white rounded border border-gray-300 focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out"></textarea>
          </div>
          <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full text-center" onclick="TampilAlert()">
  Button
        </a>
          <p class="text-xs text-gray-500 mt-3 m-1">Chicharrones blog helvetica normcore iceland tousled brook viral artisan.</p>
        </div>
      </div>
    </section>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    function ShowAlert() {
      swal({
        title: "Tidak Dapat Masuk",
        text: "Anda harus login terlebih dahulu",
        icon: "warning",
        dangerMode: true,
      })
    }
  </script>

<script>
    function TampilAlert() {
      swal({
        title: "Tidak Dapat Mengirim Feedback",
        text: "Anda harus login terlebih dahulu",
        icon: "warning",
        dangerMode: true,
      })
    }
  </script>
  <script>
          function review() {
      swal({
        title: "Tidak Dapat Melihat Feedback",
        text: "Anda harus login terlebih dahulu",
        icon: "warning",
        dangerMode: true,
      })
    }
  </script>
</body>