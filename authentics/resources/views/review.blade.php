<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Feedback</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body style="background-color: #00FA9A;">
  <ul class="nav justify-content-center bg-secondary">
  <li class="nav-item">
    <a class="nav-link text-light" href="/halhome">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="/jadwal">Make schedules</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="/rating">Contact us</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-light" href="/Review">Feedback</a>
  </li>
  <li class="nav-item">
  <a class="nav-link text-light" href="/daftar">List Schdule</a>
  </li>      
</ul>
<table class="container-fluid table mt-3 bg-dark">
  <thead>
    <tr>
      <th scope="col"class="text-light">No</th>
      <th scope="col"class="text-light">Nama</th>
      <th scope="col"class="text-light">Email</th>
      <th scope="col"class="text-light">Message</th>
    </tr>
  </thead>
  <tbody>
    @foreach($reviewList as $db)
    <tr>
      <th scope="row" class="text-light">{{ $db -> id }}</th>
      <td class="text-light">{{ $db -> nama }}</td>
      <td class="text-light">{{ $db -> email }}</td>
      <td class="text-light">{{ $db -> message }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>