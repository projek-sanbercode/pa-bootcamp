<script src="https://cdn.tailwindcss.com"></script>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Home</title>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
</head>
<body class="bg-teal-500">
  <header class="text-gray-600 body-font bg-stone-500">
    <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
      <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0" href="/utama">
        <img src="gambar/brand.png" alt="" width="50px">
        <span class="ml-3 text-xl text-gray-50">Barbershop</span>
      </a>
      <nav class="md:mr-auto md:ml-4 md:py-1 md:pl-4 md:border-l md:border-gray-400	flex flex-wrap items-center text-base justify-center">
        <a class="mr-5 hover:text-gray-900 text-gray-50" href="/halhome">Home</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50 "href="/schdule">Make schedules</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50" href="/halcontact">Contact us</a> 
      </nav>
      <button class="inline-flex text-white bg-yellow-300 border-0 py-1 px-4 focus:outline-none hover:bg-yellow-300 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-yellow-300 duration-300 transition ease-in-out delay-150 bg-yellow-300 hover:-translate-y-1 hover:scale-110 hover:bg-yellow-300 duration-300 m-1"><a href="{{ route('logout') }}">Log out</a></button>
    </div>
  </header>
  <section class="text-gray-600 body-font">
    <div class="container px-5 py-24 mx-auto flex flex-col">
      <div class="lg:w-4/6 mx-auto">
        <div class="rounded-lg h-64 overflow-hidden">
          <img alt="content" class="object-cover object-center h-full w-full" src="gambar/barbershop-logo-png-transparent.png">
        </div>
        <div class="flex flex-col sm:flex-row mt-10">
          <div class="sm:w-1/3 text-center sm:pr-8 sm:py-8">
            <div class="w-20 h-20 rounded-full inline-flex items-center justify-center bg-gray-200 text-gray-400">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-10 h-10" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <div class="flex flex-col items-center text-center justify-center">
              <h2 class="font-medium title-font mt-4 text-gray-900 text-lg">Sejarah Barbershop</h2>
              <div class="w-12 h-1 bg-indigo-500 rounded mt-2 mb-4"></div>
              <p class="text-base text-zinc-100">Sejarah Barbershop di Dunia maupun di Indonesia </p>
            </div>
          </div>
          <div class="sm:w-2/3 sm:pl-8 sm:py-8 sm:border-l border-gray-200 sm:border-t-0 border-t mt-4 pt-4 sm:mt-0 text-center sm:text-left">
            <p class="leading-relaxed text-lg mb-4 text-zinc-300">Barbershop memiliki sejarah yang panjang baik di Indonesia maupun di dunia. Barbershop cukup dekat kaitannya dengan potong atau pangkas rambut. Menurut beberapa sumber, potong rambut disebut sudah muncul sejak zaman purba, bahkan sebelum Robert Hincliffe menemukan gunting pada 1761 di Inggris.
              <br>
              Di Indonesia, tapak tilas tukang pangkas rambut bisa ditemukan dalam dokumentasi foto tempo dulu milik Koninklijk Instituut voor Taal-, Land-en Volkenkunde (KITLV) yang berada di Leiden, Belanda. Pada periode 1911-1930, ada foto orang Madura di Surabaya yang menjadi tukang cukur. Serta, ada foto tukang cukur asal Tiongkok di Medan.</p>
            <a class="text-indigo-500 inline-flex items-center" href="https://id.wikipedia.org/wiki/Tukang_cukur">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="text-gray-600 body-font">
    <div class="container px-5 py-24 mx-auto">
      <div class="flex flex-wrap w-full mb-20">
        <div class="lg:w-1/2 w-full mb-6 lg:mb-0">
          <div data-aos="fade-right">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">Para penata rambut dengan bayaran tertinggi</h1>
          </div>
          <div class="h-1 w-20 bg-indigo-500 rounded"></div>
        </div>
        <p class="lg:w-1/2 w-full leading-relaxed text-gray-500 text-zinc-200">Ternyata ada para penata rambut yang mempunyai bayaran sangat tinggi sehingga para awam yang mendengarkannya antara percaya dan tidak percaya dikarenakan nominal bayaran penata rambu tersebut terlalu tinggi</p>
      </div>
      <div class="flex flex-wrap -m-4">
        <div class="xl:w-1/4 md:w-1/2 p-4">
          <div class="bg-gray-100 p-6 rounded-lg">
            <img class="h-40 rounded w-full object-cover object-center mb-6" src="gambar/Oribe_Slider.jpg" alt="content">
            <h3 class="tracking-widest text-indigo-500 text-xs font-medium title-font">Barbershop</h3>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4">Oribe Canales</h2>
            <p class="leading-relaxed text-base">Oribe Canales merupakan penata rambut terkenal asal Kuba. Oribe merupakan salah satu penata rambut yang pernah bekerja sama dengan Jennifer Lopez. Karirnya dimulai sebagai asisten dari ikon penata rambut, Garren, saat dia membuka salon pertamanya di Plaza Hotel, New York. Untuk mendapatkan jasa dari Oribe, kamuharus mengeluarkan biaya sebesar $400 atau setara dengan Rp 5 juta. Selain menata rambut, Oribe juga memiliki brand produk kesehatan rambut.</p>
          </div>
        </div>
        <div class="xl:w-1/4 md:w-1/2 p-4">
          <div class="bg-gray-100 p-6 rounded-lg">
            <img class="h-40 rounded w-full object-cover object-center mb-6" src="gambar/stuartphillips.jpg" alt="content">
            <h3 class="tracking-widest text-indigo-500 text-xs font-medium title-font">Barbershop</h3>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4">Stuart Phillips</h2>
            <p class="leading-relaxed text-base">Stuart Phillips adalah penata rambut asal Inggris yang mempunyai banyak pelanggan setia di kalangan artis, seperti Jamie Oliver, Jean Claude Van Damme, Benicio Del Toro, dan pengusaha properti asal Italia Beverley Lateo. Phillips bahkan pernah mendapatkan penghargaan Best Luxury Hair Salon in the UK, Best Health and Beauty Firm in the UK, dan Sustained Excellence in Hairdressing Services in the UK dari Wealth and Finance International Business Awards 2016.</p>
          </div>
        </div>
        <div class="xl:w-1/4 md:w-1/2 p-4">
          <div class="bg-gray-100 p-6 rounded-lg">
            <img class="h-40 rounded w-full object-cover object-center mb-6" src="gambar/rosanno.jpeg" alt="content">
            <h3 class="tracking-widest text-indigo-500 text-xs font-medium title-font">Barbershop</h3>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4"> Rossano Ferretti</h2>
            <p class="leading-relaxed text-base">Rossano Ferretti merupakan penata rambut yang sering bekerja sama untuk merk-merk terkenal seperti Armani, Dior, dan Yves Saint Laurent. Selain untuk penataan rambut, Rossano Ferretti dikenal atas produk-produk kesehatan rambut mahal seperti seperti shampoo, conditioner, masker rambut, serum, dan produk-produk kesehatan rambut mahal serta berkualitas lainnya. Salah satu pelanggan setianya adalah Angelina Jolie dan masih banyak pelanggan lainnya.</p>
          </div>
        </div>
        <div class="xl:w-1/4 md:w-1/2 p-4">
          <div class="bg-gray-100 p-6 rounded-lg">
            <img class="h-40 rounded w-full object-cover object-center mb-6" src="gambar/tedgibson.jpg" alt="content">
            <h3 class="tracking-widest text-indigo-500 text-xs font-medium title-font">Barbershop</h3>
            <h2 class="text-lg text-gray-900 font-medium title-font mb-4">Ted Gibson</h2>
            <p class="leading-relaxed text-base">Ted Gibson adalah penata rambut yang telah berkarya selama 13 tahun. Saking populernya, Gibson harus menaikkan tarif dari $950 atau setara dengan Rp 12 juta, ke $1,200 atau setara dengan Rp 16 juta.  Renee Zellweger dan Anne Hathaway merupakan pelanggan setia Gibson. Namun, tahun lalu Gibson mengejutkan publik dengan menutup salonnya di Manhattan's Flatiron. Gibson belum berencana untuk kembali membuka salonnya.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="text-gray-600 body-font">
    <div class="container px-5 py-24 mx-auto">
      <div class="flex flex-col">
        <div class="h-1 bg-gray-200 rounded overflow-hidden">
          <div class="w-24 h-full bg-indigo-500"></div>
        </div>
        <div class="flex flex-wrap sm:flex-row flex-col py-6 mb-12">
          <h1 class="sm:w-2/5 text-gray-900 font-medium title-font text-2xl mb-2 sm:mb-0">Model rambut populer</h1>
          <p class="sm:w-3/5 leading-relaxed text-base sm:pl-10 pl-0">Mulai dari gaya rambut pendek hingga panjang, beberapa model banyak diminati oleh pria hingga menjadi tren yang bertahan lama.</p>
        </div>
      </div>
      <div class="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
        <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
          <div data-aos="flip-right">
            <div class="rounded-lg h-64 overflow-hidden">
              <img alt="content" class="object-cover object-center h-full w-full" src="gambar/crew-cut-haircuts.jpg">
            </div>
            <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Crew Cut Fade Mens Hair</h2>
            <p class="text-base leading-relaxed mt-2">Gaya crew cut ini menjadi salah satu gaya rambut pria yang cukup populer dan trendi di generasi z. Tampilannya menjadi pilihan yang mencolok dan modern untuk meningkatkan pesona pria. Potongan cepaknya dapat dibiarkan memanjang atau pendek sehingga dapat menyesuaikan kembali pada selera pria masing-masing.</p>
          </div>
          </div>
        <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
          <div class="rounded-lg h-64 overflow-hidden">
            <img alt="content" class="object-cover object-center h-full w-full" src="gambar/69ce5c4c3856bc2075f2ab3d93638ac4.jpg">
          </div>
          <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Taper Haircut + Brushed Up</h2>
          <p class="text-base leading-relaxed mt-2">Untuk melengkapi gaya rambut yang disikat ke atas, semua jenis potongan melancip dapat menyempurnakan gaya tersebut hingga tampak trendi. Jika ingin mendapatkan tampilan yang lebih tajam dan mencolok, pilih potongan rambut lancip dan tinggi. Sedangkan untuk penampilan yang lebih sederhana, dapat memilih potongan lancip rendah.</p>
        </div>
        <div class="p-4 md:w-1/3 sm:mb-0 mb-6">
          <div class="rounded-lg h-64 overflow-hidden">
            <img alt="content" class="object-cover object-center h-full w-full" src="gambar/Flow1.jpg">
          </div>
          <h2 class="text-xl font-medium title-font text-gray-900 mt-5">Flow Hairstyles with Short Sides</h2>
          <p class="text-base leading-relaxed mt-2">Ekspresikan pesona yang trendi dan modern dengan gaya rambut panjang yang cocok untuk pria. Gaya ini akan tampak keren dan stylish pada tahun 2021 dan tidak membutuhkan banyak gaya atau perawatan. Potongan garis di sepanjang dahi dan pelipis akan menyempurnakan gaya trendi ini.</p>
        </div>
      </div>
    </div>
    <!--Pemesanan-->
    <div data-aos="zoom-in">
      <section class="text-gray-600 body-font">
        <div class="container px-5 py-24 mx-auto flex flex-wrap">
          <h2 class="sm:text-3xl text-2xl text-gray-900 font-medium title-font mb-2 md:w-2/5">Buat jadwal anda  sekarang</h2>
          <div class="md:w-3/5 md:pl-6">
            <p class="leading-relaxed text-base text-zinc-300">Agar teman-teman sekalian tidak mengantri maka teman-teman sekalian bisa membuat jadwal kepada para barbers kami dengan melalui website ini.</p>
            <div class="flex md:mt-4 mt-6">
              <button class="inline-flex text-white bg-indigo-500 border-0 py-1 px-4 focus:outline-none hover:bg-indigo-600 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300 transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300"><a href="/schdule" class="text-decoratio-none text-light">Make Schdules</a></button>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!--Footer-->
    <footer class="text-gray-600 body-font">
      <div class="container px-5 py-8 mx-auto flex items-center sm:flex-row flex-col">
        <a class="flex title-font font-medium items-center md:justify-start justify-center text-gray-900">
          <img src="gambar/brand.png" alt="" width="50px">
          <span class="ml-3 text-xl">Barbershop</span>
        </a>
        <p class="text-sm text-gray-500 sm:ml-4 sm:pl-4 sm:border-l-2 sm:border-gray-200 sm:py-2 sm:mt-0 mt-4">© 2023 Barbershop —
          <a href="https://twitter.com/knyttneve" class="text-gray-600 ml-1" rel="noopener noreferrer" target="_blank">@Barbershop</a>
        </p>
        <span class="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start">
          <a class="text-gray-500">
            <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
              <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
            </svg>
          </a>
          <a class="ml-3 text-gray-500">
            <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
              <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
            </svg>
          </a>
          <a class="ml-3 text-gray-500">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
              <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
              <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
            </svg>
          </a>
          <a class="ml-3 text-gray-500">
            <svg fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="0" class="w-5 h-5" viewBox="0 0 24 24">
              <path stroke="none" d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"></path>
              <circle cx="4" cy="4" r="2" stroke="none"></circle>
            </svg>
          </a>
        </span>
      </div>
    </footer>


  </section>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
    function ShowAlert() {
      swal({
        title: "Tidak Dapat Masuk",
        text: "Anda harus login terlebih dahulu",
        icon: "warning",
        dangerMode: true,
      })
    }
  </script>
  </div>
</body>
</html>