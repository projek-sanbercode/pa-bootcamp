<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Locate</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="bg-zinc-400">
<header class="text-gray-600 body-font bg-stone-500">
    <div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
      <a class="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
        <img src="gambar/brand.png" alt="" width="50px">
        <span class="ml-3 text-xl text-gray-50">Barbershop</span>
      </a>
      <nav class="md:mr-auto md:ml-4 md:py-1 md:pl-4 md:border-l md:border-gray-400	flex flex-wrap items-center text-base justify-center">
        <a class="mr-5 hover:text-gray-900 text-gray-50 " href="/utama">Home</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50 " onclick="ShowAlert()">Make schedules</a>
        <a class="mr-5 hover:text-gray-900 text-gray-50" href="/contactus">Contact us</a>
      </nav>
      <button class="inline-flex text-white bg-yellow-300 border-0 py-1 px-4 focus:outline-none hover:bg-yellow-300 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-yellow-300 duration-300 transition ease-in-out delay-150 bg-yellow-300 hover:-translate-y-1 hover:scale-110 hover:bg-yellow-300 duration-300 m-1"><a href="/login">Login</a></button>
      <button class="inline-flex text-white bg-indigo-500 border-0 py-1 px-4 focus:outline-none hover:bg-indigo-600 rounded transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-indigo-500 duration-300 transition ease-in-out delay-150 bg-blue-500 hover:-translate-y-1 hover:scale-110 hover:bg-teal-500 duration-300"><a href="/register">Register</a></button>
    </div>
  </header>
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d127427.2781065312!2d98.692339!3d3.563886!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4b0b4386e7ff3fa0!2sPerabot%20Agung!5e0!3m2!1sid!2sid!4v1672757389857!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</body>
</html>