<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class SchduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schdule')->insert([
            'nama' => 'Johannes Sipayung',
            'tanggal' => '5/1/2023',
            'email' => 'johannesssipayung27@gmail.com',
            'barbers' => 'bastian',
        ]);
    }
}
