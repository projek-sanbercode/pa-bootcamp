<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class feedbackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feedback')->insert([
            'nama' => 'Johannes Sipayung', 
            'email' => 'johannesssipayung27@gmail.com',
            'message' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sequi, quo explicabo consequatur officiis atque qui nemo iste voluptatem laborum praesentium excepturi, et quasi at iure placeat dolore, est rerum quia!'
        ]);
    }
}
