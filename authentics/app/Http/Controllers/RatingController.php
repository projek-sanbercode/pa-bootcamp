<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rating;
class RatingController extends Controller
{
    public function rating() {
        return view('rating');
    }
    
    public function tambahrating(Request $request) {
        // dd($request->all());
        Rating::create($request->all());
        // return redirect()->route('/halhome');
        return view('/halhome');
    }

    public function halhome() {
        return view('/halhome');
    }
}
