<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;

class FeedbackController extends Controller
{
    public function feed() {
        return view('halcontact');
    }

    public function tambahdata(Request $request){
        
        Feedback::create($request->all());
        return redirect()->route('/halhome');
    }
}
