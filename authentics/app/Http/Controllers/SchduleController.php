<?php

namespace App\Http\Controllers;

use App\Models\Schdule;
use Illuminate\Http\Request;

class SchduleController extends Controller
{
    public function index() {
        return view('schdule');
    }

    public function insertdata(Request $request) {
        Schdule::create($request->all());
        return redirect('halhome');
    }

    public function halhome() {
        return view('halhome');
    }
}
