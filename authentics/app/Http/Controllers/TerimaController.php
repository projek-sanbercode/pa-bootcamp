<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schdule;

class TerimaController extends Controller
{
    public function tampil($id)
    {   $data = Schdule::find($id);
        return view ('layout.accept' , compact('data'));
    }

    public function update(Request $request , $id)
    {
        $data = Schdule::find($id);
        $data->update($request->all());
        return redirect('master');
    }

    public function Tolak($id)
    {   $data = Schdule::find($id);
        return view('layout.tolak' , compact('data'));
    }

    public function reject(Request $request , $id)
    {
        $data = Schdule::find($id);
        $data->update($request->all());
        return redirect('master');
    }

}
