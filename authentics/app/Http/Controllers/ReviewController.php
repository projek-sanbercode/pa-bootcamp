<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rating;
use App\Models\Schdule;

class ReviewController extends Controller
{
    public function review() {
        $review = Rating::all();
        return view('/review', ['reviewList' => $review] );
    }
    public function daftar()
    {   $data = Schdule::all();
        return view('list' , compact('data'));
    }
}
