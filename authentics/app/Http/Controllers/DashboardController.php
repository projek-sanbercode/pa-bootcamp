<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\jadwal;
use App\Models\Schdule;

class DashboardController extends Controller
{
        public function index()
        {
            $data = jadwal::all();
            return view('layout.dashboard' , compact('data'));
        }

        public function utama()
        {   
            $data = jadwal::all();
            $total = jadwal::count();
            $tes = Schdule::count();
            return view('layout.master' , compact('data' , 'total' , 'tes'));
        }
}
