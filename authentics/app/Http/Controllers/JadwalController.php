<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\jadwal;

class JadwalController extends Controller
{
    public function index()
    {
        return view('jadwal');
    }
    public function tambo(Request $request)
    {
        jadwal::create($request->all());
        return redirect('halhome');
    }
}
